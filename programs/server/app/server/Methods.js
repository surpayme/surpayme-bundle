(function(){Meteor.methods({
	removeAll:function(){
		Products.remove({});
		Preguntas.remove({});
		Ventas.remove({});
	},
	
	removePreguntas: function(){
		Preguntas.remove({});
	},
	
	productsInsert: function(userInfo, sellerData, name, desc, price, pais, cantidad, estado, ciudad, catName, fechaPublicacion, foto1, foto2, foto3, foto4){
		Products.insert({
			sellerData: userInfo,
		    name: name,
		    desc: desc,
		    price: price,
		    pais: pais,
		    cantidad: cantidad,
		    estado: estado,
		    ciudad: ciudad,
		    catName: catName,
		    fechaPublicacion: new Date(),
		    foto1: foto1,
		    foto2: foto2,
		    foto3: foto3,
		    foto4: foto4
		}, function(err){
			if(err){
				return err;
			}else{
				return true;
			}
		});
	},

	preguntasInsert: function(pregunta, usuario, fotoUser, productID){
		Preguntas.insert({
			pregunta: pregunta,
	        usuario: usuario,
	        fotoUser: fotoUser,
	        productID: productID,
	        respondida: false,
	        respuesta: ""
		});
	},

	ventasInsert: function(){

	},


});

})();
