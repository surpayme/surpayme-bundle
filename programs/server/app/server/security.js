(function(){//Politicas de seguridad para la gestión de las colleciones de la app

Products.permit(['insert', 'remove', 'update']).ifLoggedIn().apply();

Preguntas.permit(['insert', 'update']).ifLoggedIn().apply();

SubCategories.permit(['insert', 'remove', 'update']).never().apply();

Categories.permit(['insert', 'remove', 'update']).never().apply();

Paises.permit(['insert', 'remove', 'update']).never().apply();

Ventas.permit(['insert', 'update']).ifLoggedIn().apply();


Imagenes.allow({
	
	insert: function() { 

			return true; 

	},
	
	update: function() { 
		if(Meteor.user()){
			return true 
		}
	},
	
	remove: function() { 
		return false 
	},
	
	download: function(){ 
		return true 
	}

});

Meteor.users.permit(['insert', 'update']).apply();

})();
