(function(){  Router.route('registrarse', {
    path: '/registrarse',
    template: 'registrarse',
    waitOn: function() { 
      return Meteor.subscribe('usuarios'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('login', {
    path: '/login',
    template: 'login',
    waitOn: function() { 
      return Meteor.subscribe('usuarios'); 
    },
  }, function(){
    return GAnalytics.pageview();
  });

})();
