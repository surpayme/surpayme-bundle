(function(){Router.configure({
  layoutTemplate: "layout",
  yieldTemplates: {
    'products': {
      to: 'products'
    },
    "categories":{
      to: "categories"
    },
    "footer": {
      to: "footer"
    }
  },
  loadingTemplate: "loading",
});

})();
