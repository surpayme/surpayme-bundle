(function(){Router.route('confirmacion', {
    path: "/compra/confirmacion",
    template: "confirmacion"
  }, function(){
    return GAnalytics.pageview();
});

Router.route('pago', {
    path: "/compra/pago",
    template: "pago"
  }, function(){
    return GAnalytics.pageview();
});

Router.route('confirmPago', {
    path: "/compra/confirmPago",
    template: "confirmForm",
    waitOn: function() { 
      return Meteor.subscribe('ventas'); 
    }
  }, function(){
    return GAnalytics.pageview();
});

})();
