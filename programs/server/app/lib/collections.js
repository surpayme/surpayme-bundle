(function(){Categories 	  = new Meteor.Collection('categories');
SubCategories = new Meteor.Collection('subcategories');
Products 	  = new Meteor.Collection('products');
Preguntas     = new Meteor.Collection('preguntas');
Paises        = new Meteor.Collection('paises');
Ventas        = new Meteor.Collection('ventas');

Categories.initEasySearch('name');

Imagenes = new FS.Collection('imagenes', {
    stores: [new FS.Store.GridFS("imagenes")]
});

})();
