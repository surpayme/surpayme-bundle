(function(){Router.route('vender', {
    path: '/vender',
    template: 'vender',
    waitOn: function() {
      Meteor.subscribe('paises');
      Meteor.subscribe('subcategories');
      return Meteor.subscribe('categories'); 
    }
  }, function(){
    return GAnalytics.pageview();
});

Router.route('subirFotos', {
    path: '/vender/subirFotos/',
    template: 'subirFotos',
    waitOn: function() {
      return Meteor.subscribe('imagenes'); 
    }
  }, function(){
    return GAnalytics.pageview();
});

Router.route('publicar', {
    path: '/vender/publicar',
    template: 'publicar',
    waitOn: function(){
      Meteor.subscribe('imagenes');
      return Meteor.subscribe('productos');
    }
  }, function(){
    return GAnalytics.pageview();
});

})();
