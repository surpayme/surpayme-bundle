(function(){  Router.route('mainPad', {
    path: "/",
    template: "mainPad",
    waitOn: function() { 
      Meteor.subscribe('imagenes');
      return Meteor.subscribe('productos'); 
    }
  });


  Router.route('products', {
    data: function(){
      Session.set('category', Router.params.name);
    },
    waitOn: function() { 
      Meteor.subscribe('imagenes');
      return Meteor.subscribe('productos'); 
    },
    template: 'products',
    path: '/products/:name'
  }, function(){
    return GAnalytics.pageview();
  });

  Router.route('producto', {
    data: function(){
      Session.set('productID', this.params._id);
    },
    waitOn: function() {
      Meteor.subscribe('preguntas');
      Meteor.subscribe('imagenes');
      return Meteor.subscribe('productos'); 
    },
    template: 'product',
    path: '/producto/:_id'
  }, function(){
    return GAnalytics.pageview();
  });

})();
