(function(){Router.route('busqProducto', {
    path: '/busqProducto',
    template: 'busqProducto',
    waitOn: function() {
    	Meteor.subscribe('imagenes');
     	return Meteor.subscribe('productos'); 
    },
  }, function(){
    return GAnalytics.pageview();
});

})();
