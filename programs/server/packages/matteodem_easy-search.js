(function () {

/* Imports */
var _ = Package.underscore._;
var DDP = Package.ddp.DDP;
var DDPServer = Package.ddp.DDPServer;
var Meteor = Package.meteor.Meteor;
var MongoInternals = Package.mongo.MongoInternals;
var Mongo = Package.mongo.Mongo;
var WebApp = Package.webapp.WebApp;
var main = Package.webapp.main;
var WebAppInternals = Package.webapp.WebAppInternals;
var Log = Package.logging.Log;
var Tracker = Package.deps.Tracker;
var Deps = Package.deps.Deps;
var Blaze = Package.ui.Blaze;
var UI = Package.ui.UI;
var Handlebars = Package.ui.Handlebars;
var Spacebars = Package.spacebars.Spacebars;
var check = Package.check.check;
var Match = Package.check.Match;
var Random = Package.random.Random;
var EJSON = Package.ejson.EJSON;
var HTML = Package.htmljs.HTML;

/* Package-scope variables */
var EasySearch;

(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/matteodem:easy-search/lib/easy-search-common.js                                                     //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
EasySearch = (function () {                                                                                     // 1
  'use strict';                                                                                                 // 2
                                                                                                                // 3
  var ESCounts,                                                                                                 // 4
    Searchers,                                                                                                  // 5
    indexes = {/** @see defaultOptions */},                                                                     // 6
    defaultOptions = {                                                                                          // 7
      'format' : 'mongo',                                                                                       // 8
      'skip' : 0,                                                                                               // 9
      'limit' : 10,                                                                                             // 10
      'use' : 'minimongo',                                                                                      // 11
      'reactive' : true,                                                                                        // 12
      'props' : {},                                                                                             // 13
      'transform' : function () {},                                                                             // 14
      'sort' : function () {                                                                                    // 15
        if (Searchers[this.use]) {                                                                              // 16
          return Searchers[this.use].defaultSort(this);                                                         // 17
        }                                                                                                       // 18
                                                                                                                // 19
        return {};                                                                                              // 20
      },                                                                                                        // 21
      'reactiveSort' : function () {                                                                            // 22
        if ('minimongo' === this.use || 'mongo-db' === this.use) {                                              // 23
          return this.sort();                                                                                   // 24
        }                                                                                                       // 25
      },                                                                                                        // 26
      'count' : function () {                                                                                   // 27
        var doc = ESCounts.findOne({ _id : this.name });                                                        // 28
                                                                                                                // 29
        if (doc) {                                                                                              // 30
          return doc.count;                                                                                     // 31
        }                                                                                                       // 32
                                                                                                                // 33
        return 0;                                                                                               // 34
      },                                                                                                        // 35
      'changeResults' : function (results) {                                                                    // 36
        return results;                                                                                         // 37
      },                                                                                                        // 38
      /**                                                                                                       // 39
       * When using elastic-search it's the query object,                                                       // 40
       * while using with mongo-db it's the selector object.                                                    // 41
       *                                                                                                        // 42
       * @param {String} searchString                                                                           // 43
       * @param {Object} options                                                                                // 44
       * @return {Object}                                                                                       // 45
       */                                                                                                       // 46
      'query' : function (searchString, options) {                                                              // 47
        return Searchers[this.use].defaultQuery(this, searchString);                                            // 48
      }                                                                                                         // 49
    };                                                                                                          // 50
                                                                                                                // 51
  ESCounts = new Mongo.Collection('esCounts');                                                                  // 52
                                                                                                                // 53
  /** Helper Functions */                                                                                       // 54
  function setUpPublication(name, opts) {                                                                       // 55
    Meteor.publish(name + '/easySearch', function (conf) {                                                      // 56
      var resultSet,                                                                                            // 57
        findOptions = {},                                                                                       // 58
        resultArray = [],                                                                                       // 59
        publishScope = this,                                                                                    // 60
        publishHandle;                                                                                          // 61
                                                                                                                // 62
      check(conf, Object);                                                                                      // 63
                                                                                                                // 64
      // TODO: sanity check each property                                                                       // 65
      indexes[name].skip = conf.skip;                                                                           // 66
      indexes[name].limit = conf.limit || indexes[name].limit;                                                  // 67
      indexes[name].props = _.extend(indexes[name].props, conf.props);                                          // 68
      indexes[name].publishScope = this;                                                                        // 69
                                                                                                                // 70
      resultSet = Searchers[opts.use].search(name, conf.value, indexes[name]);                                  // 71
                                                                                                                // 72
      ESCounts.update({ _id: name }, { $set: { count: resultSet.total } }, { upsert: true });                   // 73
                                                                                                                // 74
      if (!resultSet.results.length) return this.ready();                                                       // 75
                                                                                                                // 76
      if (_.isObject(resultSet.results[0])) {                                                                   // 77
        resultArray = _.pluck(resultSet.results, '_id');                                                        // 78
      } else if (_.isString(resultSet.results[0])) {                                                            // 79
        resultArray = resultSet.results;                                                                        // 80
      }                                                                                                         // 81
                                                                                                                // 82
      // properly observe the collection!                                                                       // 83
      if (opts.returnFields) {                                                                                  // 84
        findOptions.fields = EasySearch._transformToFieldSpecifiers(opts.returnFields);                         // 85
      }                                                                                                         // 86
                                                                                                                // 87
      // see http://stackoverflow.com/questions/3142260/order-of-responses-to-mongodb-in-query                  // 88
      resultArray = _.map(resultArray, function (id) {                                                          // 89
        return { _id: id };                                                                                     // 90
      });                                                                                                       // 91
                                                                                                                // 92
      publishHandle = opts.collection                                                                           // 93
        .find({ $or: resultArray }, findOptions)                                                                // 94
        .observe({                                                                                              // 95
          added: function (doc) {                                                                               // 96
            doc._index = name;                                                                                  // 97
            publishScope.added('esSearchResults', doc._id, doc);                                                // 98
          },                                                                                                    // 99
          changed: function (doc) {                                                                             // 100
            publishScope.changed('esSearchResults', doc._id, doc);                                              // 101
          },                                                                                                    // 102
          removed: function (doc) {                                                                             // 103
            publishScope.removed('esSearchResults', doc._id);                                                   // 104
          }                                                                                                     // 105
        }                                                                                                       // 106
      );                                                                                                        // 107
                                                                                                                // 108
      publishScope.onStop(function () {                                                                         // 109
        publishHandle.stop();                                                                                   // 110
      });                                                                                                       // 111
                                                                                                                // 112
      publishScope.ready();                                                                                     // 113
    });                                                                                                         // 114
                                                                                                                // 115
    Meteor.publish(name + '/easySearchCount', function () {                                                     // 116
      return ESCounts.find({ '_id' : name });                                                                   // 117
    });                                                                                                         // 118
  }                                                                                                             // 119
                                                                                                                // 120
  function extendTransformFunction(collection, originalTransform) {                                             // 121
    return function (doc) {                                                                                     // 122
      var transformedDoc = collection._transform(doc);                                                          // 123
      return _.isFunction(originalTransform) ? originalTransform(transformedDoc) : transformedDoc;              // 124
    };                                                                                                          // 125
  }                                                                                                             // 126
                                                                                                                // 127
  if (Meteor.isClient) {                                                                                        // 128
    /**                                                                                                         // 129
     * find method to let users interact with search results.                                                   // 130
     * @param {Object} selector                                                                                 // 131
     * @param {Object} options                                                                                  // 132
     * @returns {MongoCursor}                                                                                   // 133
     */                                                                                                         // 134
    defaultOptions.find = function (selector, options) {                                                        // 135
      selector = selector || {};                                                                                // 136
      selector._index = this.name;                                                                              // 137
                                                                                                                // 138
      if (this.collection._transform) {                                                                         // 139
        options.transform = extendTransformFunction(this.collection, options.transform);                        // 140
      }                                                                                                         // 141
                                                                                                                // 142
      return ESSearchResults.find(selector, options);                                                           // 143
    };                                                                                                          // 144
                                                                                                                // 145
    /**                                                                                                         // 146
     * findOne method to let users interact with search results.                                                // 147
     * @param {Object} selector                                                                                 // 148
     * @param {Object} options                                                                                  // 149
     * @returns {Document}                                                                                      // 150
     */                                                                                                         // 151
    defaultOptions.findOne = function (selector, options) {                                                     // 152
      if (_.isObject(selector) || !selector) {                                                                  // 153
        selector = selector || {};                                                                              // 154
        selector._index = this.name;                                                                            // 155
      }                                                                                                         // 156
                                                                                                                // 157
      if (this.collection._transform) {                                                                         // 158
        options.transform = extendTransformFunction(this.collection, options.transform);                        // 159
      }                                                                                                         // 160
                                                                                                                // 161
      return ESSearchResults.findOne(selector, options);                                                        // 162
    };                                                                                                          // 163
  }                                                                                                             // 164
                                                                                                                // 165
                                                                                                                // 166
  /**                                                                                                           // 167
   * Searchers contains all engines that can be used to search content, until now:                              // 168
   *                                                                                                            // 169
   * minimongo (client): Client side collection for reactive search                                             // 170
   * elastic-search (server): Elastic search server to search with (fast)                                       // 171
   * mongo-db (server): MongoDB on the server to search (more convenient)                                       // 172
   *                                                                                                            // 173
   */                                                                                                           // 174
  Searchers = {};                                                                                               // 175
                                                                                                                // 176
  return {                                                                                                      // 177
    /**                                                                                                         // 178
     * Placeholder config method.                                                                               // 179
     *                                                                                                          // 180
     * @param {Object} newConfig                                                                                // 181
     */                                                                                                         // 182
    'config' : function (newConfig) {                                                                           // 183
      return {};                                                                                                // 184
    },                                                                                                          // 185
    /**                                                                                                         // 186
     * Simple logging method.                                                                                   // 187
     *                                                                                                          // 188
     * @param {String} message                                                                                  // 189
     * @param {String} type                                                                                     // 190
     */                                                                                                         // 191
    'log' : function (message, type) {                                                                          // 192
      type = type || 'log';                                                                                     // 193
                                                                                                                // 194
      if (console && _.isFunction(console[type])) {                                                             // 195
        console[type](message);                                                                                 // 196
      } else if (console && _.isFunction(console.log)) {                                                        // 197
        console.log(message);                                                                                   // 198
      }                                                                                                         // 199
    },                                                                                                          // 200
    /**                                                                                                         // 201
     * Create a search index.                                                                                   // 202
     *                                                                                                          // 203
     * @param {String} name                                                                                     // 204
     * @param {Object} options                                                                                  // 205
     */                                                                                                         // 206
    'createSearchIndex' : function (name, options) {                                                            // 207
      check(name, Match.OneOf(String, null));                                                                   // 208
      check(options, Object);                                                                                   // 209
                                                                                                                // 210
      options.name = name;                                                                                      // 211
      options.field = _.isArray(options.field) ? options.field : [options.field];                               // 212
      indexes[name] = _.extend(_.clone(defaultOptions), options);                                               // 213
                                                                                                                // 214
      options = indexes[name];                                                                                  // 215
                                                                                                                // 216
      if (options.permission) {                                                                                 // 217
        EasySearch.log(                                                                                         // 218
            'permission property is now deprecated! Return false inside a custom query method instead',         // 219
            'warn'                                                                                              // 220
        );                                                                                                      // 221
      }                                                                                                         // 222
                                                                                                                // 223
      if (Meteor.isServer && EasySearch._usesSubscriptions(name)) {                                             // 224
        setUpPublication(name, indexes[name]);                                                                  // 225
      }                                                                                                         // 226
                                                                                                                // 227
      Searchers[options.use] && Searchers[options.use].createSearchIndex(name, options);                        // 228
    },                                                                                                          // 229
    /**                                                                                                         // 230
     * Perform a search.                                                                                        // 231
     *                                                                                                          // 232
     * @param {String} name             the search index                                                        // 233
     * @param {String} searchString     the string to be searched                                               // 234
     * @param {Object} options          defined with createSearchIndex                                          // 235
     * @param {Function} callback       optional callback to be used                                            // 236
     */                                                                                                         // 237
    'search' : function (name, searchString, options, callback) {                                               // 238
      var results,                                                                                              // 239
        index = indexes[name],                                                                                  // 240
        searcherType = index.use;                                                                               // 241
                                                                                                                // 242
      check(name, String);                                                                                      // 243
      check(searchString, String);                                                                              // 244
      check(options, Object);                                                                                   // 245
      check(callback, Match.Optional(Function));                                                                // 246
                                                                                                                // 247
      if ("undefined" === typeof Searchers[searcherType]) {                                                     // 248
        throw new Meteor.Error(500, "Couldnt search with type: '" + searcherType + "'");                        // 249
      }                                                                                                         // 250
                                                                                                                // 251
      results = Searchers[searcherType].search(name, searchString, _.extend(indexes[name], options), callback); // 252
                                                                                                                // 253
      return index.changeResults(results);                                                                      // 254
    },                                                                                                          // 255
    /**                                                                                                         // 256
     * Retrieve a specific index configuration.                                                                 // 257
     *                                                                                                          // 258
     * @param {String} name                                                                                     // 259
     * @return {Object}                                                                                         // 260
     * @api public                                                                                              // 261
     */                                                                                                         // 262
    'getIndex' : function (name) {                                                                              // 263
      return indexes[name];                                                                                     // 264
    },                                                                                                          // 265
    /**                                                                                                         // 266
     * Retrieve all index configurations                                                                        // 267
     */                                                                                                         // 268
    'getIndexes' : function () {                                                                                // 269
      return indexes;                                                                                           // 270
    },                                                                                                          // 271
    /**                                                                                                         // 272
     * Retrieve a specific Seacher.                                                                             // 273
     *                                                                                                          // 274
     * @param {String} name                                                                                     // 275
     * @return {Object}                                                                                         // 276
     * @api public                                                                                              // 277
     */                                                                                                         // 278
    'getSearcher' : function (name) {                                                                           // 279
      return Searchers[name];                                                                                   // 280
    },                                                                                                          // 281
    /**                                                                                                         // 282
     * Retrieve all Searchers.                                                                                  // 283
     */                                                                                                         // 284
    'getSearchers' : function () {                                                                              // 285
      return Searchers;                                                                                         // 286
    },                                                                                                          // 287
    /**                                                                                                         // 288
     * Loop through the indexes and provide the configuration.                                                  // 289
     *                                                                                                          // 290
     * @param {Array|String} indexes                                                                            // 291
     * @param callback                                                                                          // 292
     */                                                                                                         // 293
    'eachIndex' : function (indexes, callback) {                                                                // 294
      indexes = !_.isArray(indexes) ? [indexes] : indexes;                                                      // 295
                                                                                                                // 296
      _.each(indexes, function (index) {                                                                        // 297
        callback(index, EasySearch.getIndex(index));                                                            // 298
      });                                                                                                       // 299
    },                                                                                                          // 300
    /**                                                                                                         // 301
     * Makes it possible to override or extend the different                                                    // 302
     * types of search to use with EasySearch (the "use" property)                                              // 303
     * when using EasySearch.createSearchIndex()                                                                // 304
     *                                                                                                          // 305
     * @param {String} key      Type, e.g. mongo-db, elastic-search                                             // 306
     * @param {Object} methods  Methods to be used, only 2 are required:                                        // 307
     *                          - createSearchIndex (name, options)                                             // 308
     *                          - search (name, searchString, [options, callback])                              // 309
     *                          - defaultQuery (options, searchString)                                          // 310
     *                          - defaultSort (options)                                                         // 311
     */                                                                                                         // 312
    'createSearcher' : function (key, methods) {                                                                // 313
      check(key, String);                                                                                       // 314
      check(methods.search, Function);                                                                          // 315
      check(methods.createSearchIndex, Function);                                                               // 316
                                                                                                                // 317
      Searchers[key] = methods;                                                                                 // 318
    },                                                                                                          // 319
    /**                                                                                                         // 320
     * Helper to check if searcher uses server side subscriptions for searching.                                // 321
     *                                                                                                          // 322
     * @param {String} index Index name to check configuration for                                              // 323
     */                                                                                                         // 324
    '_usesSubscriptions' : function (index) {                                                                   // 325
      var conf = EasySearch.getIndex(index);                                                                    // 326
      return conf && conf.reactive && conf.use !== 'minimongo';                                                 // 327
    },                                                                                                          // 328
    /**                                                                                                         // 329
     * Helper to transform an array of fields to Meteor "Field Specifiers"                                      // 330
     *                                                                                                          // 331
     * @param {Array} fields Array of fields                                                                    // 332
     */                                                                                                         // 333
    '_transformToFieldSpecifiers' : function (fields) {                                                         // 334
      var specifiers = {};                                                                                      // 335
                                                                                                                // 336
      _.each(fields, function (field) {                                                                         // 337
        specifiers[field] = 1;                                                                                  // 338
      });                                                                                                       // 339
                                                                                                                // 340
      return specifiers;                                                                                        // 341
    }                                                                                                           // 342
  };                                                                                                            // 343
})();                                                                                                           // 344
                                                                                                                // 345
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/matteodem:easy-search/lib/easy-search-convenience.js                                                //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
Meteor.Collection.prototype.initEasySearch = function (fields, options) {                                       // 1
  if (!_.isObject(options)) {                                                                                   // 2
    options = {};                                                                                               // 3
  }                                                                                                             // 4
                                                                                                                // 5
  EasySearch.createSearchIndex(this._name, _.extend(options, {                                                  // 6
    'collection' : this,                                                                                        // 7
    'field' : fields                                                                                            // 8
  }));                                                                                                          // 9
};                                                                                                              // 10
                                                                                                                // 11
if (Meteor.isClient) {                                                                                          // 12
  jQuery.fn.esAutosuggestData = function () {                                                                   // 13
    var input = $(this);                                                                                        // 14
                                                                                                                // 15
    if (input.prop("tagName").toUpperCase() !== 'INPUT') {                                                      // 16
      return [];                                                                                                // 17
    }                                                                                                           // 18
                                                                                                                // 19
    return EasySearch.getComponentInstance({'id': input.parent().data('id'), 'index': input.parent().data('index')}).get('autosuggestSelected');
  }                                                                                                             // 21
}                                                                                                               // 22
                                                                                                                // 23
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/matteodem:easy-search/lib/searchers/mongo.js                                                        //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
var methods = {                                                                                                 // 1
  /**                                                                                                           // 2
   * Set up a search index.                                                                                     // 3
   *                                                                                                            // 4
   * @param name                                                                                                // 5
   * @param options                                                                                             // 6
   * @returns {void}                                                                                            // 7
   */                                                                                                           // 8
  'createSearchIndex' : function (name, options) {},                                                            // 9
  /**                                                                                                           // 10
   *                                                                                                            // 11
   * Perform a really simple search with mongo db.                                                              // 12
   *                                                                                                            // 13
   * @param {String} name                                                                                       // 14
   * @param {String} searchString                                                                               // 15
   * @param {Object} options                                                                                    // 16
   * @param {Function} callback                                                                                 // 17
   * @returns {Object}                                                                                          // 18
   */                                                                                                           // 19
  'search' : function (name, searchString, options, callback) {                                                 // 20
    var cursor,                                                                                                 // 21
      results,                                                                                                  // 22
      selector,                                                                                                 // 23
      cursorOptions,                                                                                            // 24
      index = EasySearch.getIndex(name);                                                                        // 25
                                                                                                                // 26
    if (!_.isObject(index)) {                                                                                   // 27
      return;                                                                                                   // 28
    }                                                                                                           // 29
                                                                                                                // 30
    options.limit = options.limit || 10;                                                                        // 31
                                                                                                                // 32
    // if several, fields do an $or search, otherwise only over the field                                       // 33
    selector = index.query(searchString, options);                                                              // 34
                                                                                                                // 35
    if (!selector) {                                                                                            // 36
      return { total: 0, results: [] };                                                                         // 37
    }                                                                                                           // 38
                                                                                                                // 39
    cursorOptions = {                                                                                           // 40
      sort : index.sort(searchString, options)                                                                  // 41
    };                                                                                                          // 42
                                                                                                                // 43
    if (options.returnFields) {                                                                                 // 44
      cursorOptions.fields = EasySearch._transformToFieldSpecifiers(options.returnFields);                      // 45
    }                                                                                                           // 46
                                                                                                                // 47
    if (options.skip) {                                                                                         // 48
      cursorOptions.skip = options.skip;                                                                        // 49
    }                                                                                                           // 50
                                                                                                                // 51
    cursor = index.collection.find(selector, cursorOptions);                                                    // 52
                                                                                                                // 53
    results = {                                                                                                 // 54
      'results' : _.first(cursor.fetch(), options.limit),                                                       // 55
      'total' : cursor.count()                                                                                  // 56
    };                                                                                                          // 57
                                                                                                                // 58
    if (_.isFunction(callback)) {                                                                               // 59
      callback(results);                                                                                        // 60
    }                                                                                                           // 61
                                                                                                                // 62
    return results;                                                                                             // 63
  },                                                                                                            // 64
  /**                                                                                                           // 65
   * The default mongo-db query - selector used for searching.                                                  // 66
   *                                                                                                            // 67
   * @param {Object} conf                                                                                       // 68
   * @param {String} searchString                                                                               // 69
   * @param {Function} regexCallback                                                                            // 70
   *                                                                                                            // 71
   * @returns {Object}                                                                                          // 72
   */                                                                                                           // 73
  'defaultQuery' : function (conf, searchString, regexCallback) {                                               // 74
    var orSelector,                                                                                             // 75
      selector = {},                                                                                            // 76
      field = conf.field,                                                                                       // 77
      stringSelector = { '$regex' : '.*' + searchString + '.*', '$options' : 'i' };                             // 78
                                                                                                                // 79
    if (_.isString(field)) {                                                                                    // 80
      selector[field] = stringSelector;                                                                         // 81
      return selector;                                                                                          // 82
    }                                                                                                           // 83
                                                                                                                // 84
    // Convert numbers if configured                                                                            // 85
    if (conf.convertNumbers && parseInt(searchString, 10) == searchString) {                                    // 86
      searchString = parseInt(searchString, 10);                                                                // 87
    }                                                                                                           // 88
                                                                                                                // 89
    if (regexCallback) {                                                                                        // 90
      stringSelector['$regex'] = regexCallback(searchString);                                                   // 91
    }                                                                                                           // 92
                                                                                                                // 93
    // Should be an array                                                                                       // 94
    selector['$or'] = [];                                                                                       // 95
                                                                                                                // 96
    _.each(field, function (fieldString) {                                                                      // 97
      orSelector = {};                                                                                          // 98
                                                                                                                // 99
      if (_.isString(searchString)) {                                                                           // 100
        orSelector[fieldString] = stringSelector;                                                               // 101
      } else if (_.isNumber(searchString)) {                                                                    // 102
        orSelector[fieldString] = searchString;                                                                 // 103
      }                                                                                                         // 104
                                                                                                                // 105
      selector['$or'].push(orSelector);                                                                         // 106
    });                                                                                                         // 107
                                                                                                                // 108
    return selector;                                                                                            // 109
  },                                                                                                            // 110
  /**                                                                                                           // 111
   * The default mongo-db sorting method used for sorting the results.                                          // 112
   *                                                                                                            // 113
   * @param {Object} conf                                                                                       // 114
   * @return array                                                                                              // 115
   */                                                                                                           // 116
  'defaultSort' : function (conf) {                                                                             // 117
    return conf.field;                                                                                          // 118
  }                                                                                                             // 119
};                                                                                                              // 120
                                                                                                                // 121
if (Meteor.isClient) {                                                                                          // 122
  EasySearch.createSearcher('minimongo', methods);                                                              // 123
} else if (Meteor.isServer) {                                                                                   // 124
  EasySearch.createSearcher('mongo-db', methods);                                                               // 125
}                                                                                                               // 126
                                                                                                                // 127
                                                                                                                // 128
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/matteodem:easy-search/lib/easy-search-server.js                                                     //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
'use strict';                                                                                                   // 1
var ElasticSearch = Npm.require('elasticsearch');                                                               // 2
                                                                                                                // 3
EasySearch._esDefaultConfig = {                                                                                 // 4
  host : 'localhost:9200'                                                                                       // 5
};                                                                                                              // 6
                                                                                                                // 7
/**                                                                                                             // 8
 * Override the config for Elastic Search.                                                                      // 9
 *                                                                                                              // 10
 * @param {object} newConfig                                                                                    // 11
 */                                                                                                             // 12
EasySearch.config = function (newConfig) {                                                                      // 13
  if ("undefined" !== typeof newConfig) {                                                                       // 14
    check(newConfig, Object);                                                                                   // 15
    this._config = _.extend(this._esDefaultConfig, newConfig);                                                  // 16
    this.ElasticSearchClient = new ElasticSearch.Client(this._config);                                          // 17
  }                                                                                                             // 18
                                                                                                                // 19
  return this._config;                                                                                          // 20
};                                                                                                              // 21
                                                                                                                // 22
/**                                                                                                             // 23
 * Get the ElasticSearchClient                                                                                  // 24
 * @see http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current                       // 25
 *                                                                                                              // 26
 * @return {ElasticSearch.Client}                                                                               // 27
 */                                                                                                             // 28
EasySearch.getElasticSearchClient = function () {                                                               // 29
  return this.ElasticSearchClient;                                                                              // 30
};                                                                                                              // 31
                                                                                                                // 32
Meteor.methods({                                                                                                // 33
  /**                                                                                                           // 34
   * Make server side search possible on the client.                                                            // 35
   *                                                                                                            // 36
   * @param {String} name                                                                                       // 37
   * @param {String} searchString                                                                               // 38
   * @param {Object} options                                                                                    // 39
   */                                                                                                           // 40
  easySearch: function (name, searchString, options) {                                                          // 41
    check(name, String);                                                                                        // 42
    check(searchString, String);                                                                                // 43
    check(options, Object);                                                                                     // 44
    return EasySearch.search(name, searchString, options);                                                      // 45
  }                                                                                                             // 46
});                                                                                                             // 47
                                                                                                                // 48
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
// packages/matteodem:easy-search/lib/searchers/elastic-search.js                                               //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                //
'use strict';                                                                                                   // 1
                                                                                                                // 2
var Future = Npm.require('fibers/future'),                                                                      // 3
  ElasticSearch = Npm.require('elasticsearch');                                                                 // 4
                                                                                                                // 5
/**                                                                                                             // 6
 * Return Elastic Search indexable data.                                                                        // 7
 *                                                                                                              // 8
 * @param {Object} doc the document to get the values from                                                      // 9
 * @return {Object}                                                                                             // 10
 */                                                                                                             // 11
function getESFields(doc) {                                                                                     // 12
  var newDoc = {};                                                                                              // 13
                                                                                                                // 14
  _.each(doc, function (value, key) {                                                                           // 15
    newDoc[key] = _.isObject(value) && !_.isArray(value) ? JSON.stringify(value) : value;                       // 16
  });                                                                                                           // 17
                                                                                                                // 18
  return newDoc;                                                                                                // 19
}                                                                                                               // 20
                                                                                                                // 21
EasySearch.createSearcher('elastic-search', {                                                                   // 22
  /**                                                                                                           // 23
   * Write a document to a specified index.                                                                     // 24
   *                                                                                                            // 25
   * @param {String} name                                                                                       // 26
   * @param {Object} doc                                                                                        // 27
   * @param {String} id                                                                                         // 28
   * @param {Object} opts                                                                                       // 29
   * @param {Object} config                                                                                     // 30
   */                                                                                                           // 31
  'writeToIndex' : function (name, doc, id, opts, config) {                                                     // 32
    var debugMode = config.debug,                                                                               // 33
        transformedDoc = opts.transform(doc);                                                                   // 34
                                                                                                                // 35
    if (_.isObject(transformedDoc)) {                                                                           // 36
      doc = transformedDoc;                                                                                     // 37
    }                                                                                                           // 38
                                                                                                                // 39
    // add to index                                                                                             // 40
    EasySearch.ElasticSearchClient.index({                                                                      // 41
      index : name.toLowerCase(),                                                                               // 42
      type : 'default',                                                                                         // 43
      id : id,                                                                                                  // 44
      body : doc                                                                                                // 45
    }, function (err, data) {                                                                                   // 46
      if (err) {                                                                                                // 47
        console.log('Had error adding a document!');                                                            // 48
        console.log(err);                                                                                       // 49
      }                                                                                                         // 50
                                                                                                                // 51
      if (debugMode && console) {                                                                               // 52
        console.log('EasySearch: Added / Replaced document to Elastic Search:');                                // 53
        console.log('EasySearch: ' + data + "\n");                                                              // 54
      }                                                                                                         // 55
    });                                                                                                         // 56
  },                                                                                                            // 57
  /**                                                                                                           // 58
   * Setup some observers on the mongo db collection provided.                                                  // 59
   *                                                                                                            // 60
   * @param {String} name                                                                                       // 61
   * @param {Object} options                                                                                    // 62
   */                                                                                                           // 63
  'createSearchIndex' : function (name, options) {                                                              // 64
    var searcherScope = this,                                                                                   // 65
      config = EasySearch.config() || {};                                                                       // 66
                                                                                                                // 67
    if ("undefined" === typeof EasySearch.ElasticSearchClient) {                                                // 68
      EasySearch.ElasticSearchClient = new ElasticSearch.Client(this._esDefaultConfig);                         // 69
    }                                                                                                           // 70
                                                                                                                // 71
    name = name.toLowerCase();                                                                                  // 72
                                                                                                                // 73
    options.collection.find().observeChanges({                                                                  // 74
      added: function (id, fields) {                                                                            // 75
        searcherScope.writeToIndex(name, getESFields(fields), id, options, config);                             // 76
      },                                                                                                        // 77
      changed: function (id) {                                                                                  // 78
        // Overwrites the current document with the new doc                                                     // 79
        searcherScope.writeToIndex(name, getESFields(options.collection.findOne(id)), id, options, config);     // 80
      },                                                                                                        // 81
      removed: function (id) {                                                                                  // 82
        EasySearch.ElasticSearchClient.delete({                                                                 // 83
          index: name,                                                                                          // 84
          type: 'default',                                                                                      // 85
          id: id                                                                                                // 86
        }, function (error, response) {                                                                         // 87
          if (config.debug) {                                                                                   // 88
            console.log('Removed document with id ( ' +  id + ' )!');                                           // 89
          }                                                                                                     // 90
        });                                                                                                     // 91
      }                                                                                                         // 92
    });                                                                                                         // 93
  },                                                                                                            // 94
  /**                                                                                                           // 95
   * Get the data out of the JSON elastic search response.                                                      // 96
   *                                                                                                            // 97
   * @param {Object} data                                                                                       // 98
   * @returns {Array}                                                                                           // 99
   */                                                                                                           // 100
  'extractJSONData' : function (data) {                                                                         // 101
    data = _.isString(data) ? JSON.parse(data) : data;                                                          // 102
                                                                                                                // 103
    var results = _.map(data.hits.hits, function (resultSet) {                                                  // 104
      var field = '_source';                                                                                    // 105
                                                                                                                // 106
      if (resultSet['fields']) {                                                                                // 107
        field = 'fields';                                                                                       // 108
      }                                                                                                         // 109
                                                                                                                // 110
      resultSet[field]['_id'] = resultSet['_id'];                                                               // 111
      return resultSet[field];                                                                                  // 112
    });                                                                                                         // 113
                                                                                                                // 114
    return {                                                                                                    // 115
      'results' : results,                                                                                      // 116
      'total' : data.hits.total                                                                                 // 117
    };                                                                                                          // 118
  },                                                                                                            // 119
  /**                                                                                                           // 120
   * Perform a search with Elastic Search, using fibers.                                                        // 121
   *                                                                                                            // 122
   * @param {String} name                                                                                       // 123
   * @param {String} searchString                                                                               // 124
   * @param {Object} options                                                                                    // 125
   * @param {Function} callback                                                                                 // 126
   * @returns {*}                                                                                               // 127
   */                                                                                                           // 128
  'search' : function (name, searchString, options, callback) {                                                 // 129
    var bodyObj,                                                                                                // 130
      that = this,                                                                                              // 131
      fut = new Future(),                                                                                       // 132
      index = EasySearch.getIndex(name);                                                                        // 133
                                                                                                                // 134
    if (!_.isObject(index)) {                                                                                   // 135
      return;                                                                                                   // 136
    }                                                                                                           // 137
                                                                                                                // 138
    bodyObj = {                                                                                                 // 139
      "query" : index.query(searchString, options)                                                              // 140
    };                                                                                                          // 141
                                                                                                                // 142
    if (!bodyObj.query) {                                                                                       // 143
      return { total: 0, results: [] };                                                                         // 144
    }                                                                                                           // 145
                                                                                                                // 146
    bodyObj.sort = index.sort(searchString, options);                                                           // 147
                                                                                                                // 148
    if (options.returnFields) {                                                                                 // 149
      if (options.returnFields.indexOf('_id') === -1 ) {                                                        // 150
        options.returnFields.push('_id');                                                                       // 151
      }                                                                                                         // 152
                                                                                                                // 153
      bodyObj.fields = options.returnFields;                                                                    // 154
    }                                                                                                           // 155
                                                                                                                // 156
    // Modify Elastic Search body if wished                                                                     // 157
    if (index.body && _.isFunction(index.body)) {                                                               // 158
      bodyObj = index.body(bodyObj, options);                                                                   // 159
    }                                                                                                           // 160
                                                                                                                // 161
    name = name.toLowerCase();                                                                                  // 162
                                                                                                                // 163
    if ("function" === typeof callback) {                                                                       // 164
      EasySearch.ElasticSearchClient.search(name, bodyObj, callback);                                           // 165
      return;                                                                                                   // 166
    }                                                                                                           // 167
                                                                                                                // 168
    // Most likely client call, return data set                                                                 // 169
    EasySearch.ElasticSearchClient.search({                                                                     // 170
      index : name,                                                                                             // 171
      body : bodyObj,                                                                                           // 172
      size : options.limit,                                                                                     // 173
      from: options.skip                                                                                        // 174
    }, function (error, data) {                                                                                 // 175
      if (error) {                                                                                              // 176
        console.log('Had an error while searching!');                                                           // 177
        console.log(error);                                                                                     // 178
        return;                                                                                                 // 179
      }                                                                                                         // 180
                                                                                                                // 181
      if ("raw" !== index.format) {                                                                             // 182
        data = that.extractJSONData(data);                                                                      // 183
      }                                                                                                         // 184
                                                                                                                // 185
      fut['return'](data);                                                                                      // 186
    });                                                                                                         // 187
                                                                                                                // 188
    return fut.wait();                                                                                          // 189
  },                                                                                                            // 190
  /**                                                                                                           // 191
   * The default ES query object used for searching the results.                                                // 192
   *                                                                                                            // 193
   * @param {Object} options                                                                                    // 194
   * @param {String} searchString                                                                               // 195
   * @return array                                                                                              // 196
   */                                                                                                           // 197
  'defaultQuery' : function (options, searchString) {                                                           // 198
    return {                                                                                                    // 199
      "fuzzy_like_this" : {                                                                                     // 200
        "fields" : options.field,                                                                               // 201
        "like_text" : searchString                                                                              // 202
      }                                                                                                         // 203
    };                                                                                                          // 204
  },                                                                                                            // 205
  /**                                                                                                           // 206
   * The default ES sorting method used for sorting the results.                                                // 207
   *                                                                                                            // 208
   * @param {Object} options                                                                                    // 209
   * @return array                                                                                              // 210
   */                                                                                                           // 211
  'defaultSort' : function (options) {                                                                          // 212
    return options.field;                                                                                       // 213
  }                                                                                                             // 214
});                                                                                                             // 215
                                                                                                                // 216
// Expose ElasticSearch API                                                                                     // 217
EasySearch.ElasticSearch = ElasticSearch;                                                                       // 218
                                                                                                                // 219
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['matteodem:easy-search'] = {
  EasySearch: EasySearch
};

})();

//# sourceMappingURL=matteodem_easy-search.js.map
