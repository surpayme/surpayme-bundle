(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;

/* Package-scope variables */
var ClassX;

(function () {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// packages/arsnebula:classx/lib/js/classx.js                                                                          //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
ClassX = {};                                                                                                           // 1
                                                                                                                       // 2
ClassX.extend = function(base, ext) {                                                                                  // 3
                                                                                                                       // 4
  var baseP = base.prototype;                                                                                          // 5
  var childP = Object.create(baseP);                                                                                   // 6
  ext.call(childP, baseP);                                                                                             // 7
                                                                                                                       // 8
  if ( !childP.hasOwnProperty("constructor")) { return childP; }                                                       // 9
  var constructor = childP.constructor;                                                                                // 10
  constructor.prototype = childP;                                                                                      // 11
  return constructor;                                                                                                  // 12
                                                                                                                       // 13
};                                                                                                                     // 14
                                                                                                                       // 15
ClassX.Exception = ClassX.extend(Error, function(base) {                                                               // 16
                                                                                                                       // 17
  this.constructor = function Exception(message) {                                                                     // 18
                                                                                                                       // 19
    this.name = this.constructor.name;                                                                                 // 20
    this.type = this.constructor.name;                                                                                 // 21
    this.message = message;                                                                                            // 22
                                                                                                                       // 23
    if (Error.captureStackTrace) {                                                                                     // 24
      Error.captureStackTrace(this, this.constructor);                                                                 // 25
    } else {                                                                                                           // 26
      var stack = new Error().stack;                                                                                   // 27
      if (typeof stack === "string") {                                                                                 // 28
        stack = stack.split("\n");                                                                                     // 29
        stack.shift();                                                                                                 // 30
        this.stack = stack.join("\n");                                                                                 // 31
      }                                                                                                                // 32
    }                                                                                                                  // 33
  }                                                                                                                    // 34
                                                                                                                       // 35
});                                                                                                                    // 36
                                                                                                                       // 37
ClassX.Class = ClassX.extend(Object, function() {                                                                      // 38
                                                                                                                       // 39
  this.__globalEvents = {};                                                                                            // 40
                                                                                                                       // 41
  this.raiseEvent = function (event, data, global) {                                                                   // 42
    function raiseEvents(target) {                                                                                     // 43
      if ( typeof target[event]  !== 'undefined' ) {                                                                   // 44
        for (var i = target[event].length - 1; i >= 0; i -= 1) {                                                       // 45
          var callback = target[event][i];                                                                             // 46
          if ( typeof callback !== 'undefined' && callback instanceof Function ) {                                     // 47
            callback(data);                                                                                            // 48
          }                                                                                                            // 49
        }                                                                                                              // 50
      }                                                                                                                // 51
    }                                                                                                                  // 52
    global = ( typeof global === 'undefined' ) ? false : true;                                                         // 53
    if ( typeof this.__globalEvents[event]  !== 'undefined' && global === true ) { raiseEvents(this.__globalEvents); } // 54
    if ( typeof this.__localEvents[event]  !== 'undefined' ) { raiseEvents(this.__localEvents); }                      // 55
  };                                                                                                                   // 56
                                                                                                                       // 57
  this.addEventListener = function (event, callback, global) {                                                         // 58
    function addEventListener(target) {                                                                                // 59
      if ( typeof target[event] === 'undefined' ) { target[event] = []; }                                              // 60
      target[event].push(callback);                                                                                    // 61
    }                                                                                                                  // 62
    global = ( typeof global === 'undefined' ) ? false : true;                                                         // 63
    if ( global === true ) { addEventListener(this.__globalEvents); }                                                  // 64
    else { addEventListener(this.__localEvents); }                                                                     // 65
  };                                                                                                                   // 66
                                                                                                                       // 67
  this.removeEventListener = function (event, callback, global) {                                                      // 68
    function removeEventListener(target) {                                                                             // 69
      if ( typeof target[event]  === 'undefined' ) { return; }                                                         // 70
      for (var i = target[event].length - 1; i >= 0; i -= 1) {                                                         // 71
        if ( target[event][i] === callback ) {                                                                         // 72
          target[event].splice(i, 1);                                                                                  // 73
          break;                                                                                                       // 74
        }                                                                                                              // 75
      }                                                                                                                // 76
    }                                                                                                                  // 77
    global = ( typeof global === 'undefined' ) ? false : true;                                                         // 78
    if ( global === true ) { removeEventListener(this.__globalEvents); }                                               // 79
    else { removeEventListener(this.__localEvents); }                                                                  // 80
  }                                                                                                                    // 81
                                                                                                                       // 82
  this.constructor = function Class() {                                                                                // 83
    this.__localEvents = {};                                                                                           // 84
  };                                                                                                                   // 85
});                                                                                                                    // 86
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['arsnebula:classx'] = {
  ClassX: ClassX
};

})();

//# sourceMappingURL=arsnebula_classx.js.map
