(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var FS = Package['cfs:base-package'].FS;
var Blaze = Package.blaze.Blaze;
var UI = Package.blaze.UI;
var Handlebars = Package.blaze.Handlebars;
var HTML = Package.htmljs.HTML;



/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['cfs:ui'] = {};

})();

//# sourceMappingURL=cfs_ui.js.map
