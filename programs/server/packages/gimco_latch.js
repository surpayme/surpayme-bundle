(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var _ = Package.underscore._;
var HTTP = Package.http.HTTP;
var Accounts = Package['accounts-base'].Accounts;
var moment = Package['momentjs:moment'].moment;
var ServiceConfiguration = Package['service-configuration'].ServiceConfiguration;

/* Package-scope variables */
var Latch, getUser, res;

(function () {

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
// packages/gimco:latch/latch-common.js                                           //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////
                                                                                  //
/*                                                                                // 1
 * Latch integration for Meteor framework                                         // 2
 * Copyright (C) 2015 Bruno Orcha García <gimcoo@gmail.com>                       // 3
 *                                                                                // 4
 * This library is free software; you can redistribute it and/or                  // 5
 * modify it under the terms of the GNU Lesser General Public                     // 6
 * License as published by the Free Software Foundation; either                   // 7
 * version 2.1 of the License, or (at your option) any later version.             // 8
 *                                                                                // 9
 * This library is distributed in the hope that it will be useful,                // 10
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                 // 11
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU              // 12
 * Lesser General Public License for more details.                                // 13
 *                                                                                // 14
 * You should have received a copy of the GNU Lesser General Public               // 15
 * License along with this library; if not, write to the Free Software            // 16
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA // 17
 */                                                                               // 18
                                                                                  // 19
if (typeof Latch === 'undefined') {                                               // 20
  Latch = {};                                                                     // 21
}                                                                                 // 22
                                                                                  // 23
getUser = function () {                                                           // 24
    var user = Meteor.user();                                                     // 25
    if (!user) {                                                                  // 26
        throw new Meteor.Error(500, 'Not logged user');                           // 27
    }                                                                             // 28
    return user;                                                                  // 29
};                                                                                // 30
////////////////////////////////////////////////////////////////////////////////////

}).call(this);






(function () {

////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
// packages/gimco:latch/latch-server.js                                           //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////
                                                                                  //
/*                                                                                // 1
 * Latch integration for Meteor framework                                         // 2
 * Copyright (C) 2015 Bruno Orcha García <gimcoo@gmail.com>                       // 3
 *                                                                                // 4
 * This library is free software; you can redistribute it and/or                  // 5
 * modify it under the terms of the GNU Lesser General Public                     // 6
 * License as published by the Free Software Foundation; either                   // 7
 * version 2.1 of the License, or (at your option) any later version.             // 8
 *                                                                                // 9
 * This library is distributed in the hope that it will be useful,                // 10
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                 // 11
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU              // 12
 * Lesser General Public License for more details.                                // 13
 *                                                                                // 14
 * You should have received a copy of the GNU Lesser General Public               // 15
 * License along with this library; if not, write to the Free Software            // 16
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA // 17
 */                                                                               // 18
                                                                                  // 19
var crypto = Npm.require("crypto");                                               // 20
                                                                                  // 21
var LATCH_HOST = "https://latch.elevenpaths.com";                                 // 22
var LATCH_BASE = "/api/0.9";                                                      // 23
var LATCH_AUTH_TEMPLATE = _.template("11PATHS <%=appId%> <%=signedData%>");       // 24
                                                                                  // 25
// {"error":{"code":201,"message":"Account not paired"}}                          // 26
var LATH_ERROR_NOT_PAIRED = 201;                                                  // 27
                                                                                  // 28
var isErrorNotPaired = function (response) {                                      // 29
    return !!response.error                                                       // 30
        && response.error.code == LATH_ERROR_NOT_PAIRED;                          // 31
};                                                                                // 32
                                                                                  // 33
var getAccountId = function (user) {                                              // 34
    if (!user.services || !user.services.latch                                    // 35
        || _.isUndefined(user.services.latch.accountId)) {                        // 36
        throw new Meteor.Error(500, 'User not paired.');                          // 37
    }                                                                             // 38
    return user.services.latch.accountId;                                         // 39
};                                                                                // 40
                                                                                  // 41
var getConfig = function () {                                                     // 42
    var config = ServiceConfiguration.configurations.findOne({service:'latch'});  // 43
    check(config, Match.ObjectIncluding({secret: Object}));                       // 44
    check(config.secret, {appId: String, secretKey: String});                     // 45
    return config.secret;                                                         // 46
};                                                                                // 47
                                                                                  // 48
var httpRequest = function (config, service, param) {                             // 49
    var path = [LATCH_BASE, service, param].join("/");                            // 50
    var url = LATCH_HOST + path;                                                  // 51
                                                                                  // 52
    var utc = moment.utc().format("YYYY-MM-DD HH:mm:ss");                         // 53
    var hmac = crypto.createHmac("sha1", config.secretKey);                       // 54
    hmac.setEncoding("base64");                                                   // 55
    hmac.write(["GET", utc, "", path].join("\n"));                                // 56
    hmac.end();                                                                   // 57
                                                                                  // 58
    var auth = LATCH_AUTH_TEMPLATE({                                              // 59
        appId: config.appId,                                                      // 60
        signedData: hmac.read()                                                   // 61
    });                                                                           // 62
    res = HTTP.get(url, {                                                         // 63
        headers: {                                                                // 64
            "Authorization": auth,                                                // 65
            "X-11Paths-Date": utc                                                 // 66
        }                                                                         // 67
    });                                                                           // 68
    return res.data;                                                              // 69
};                                                                                // 70
                                                                                  // 71
Latch.isLocked = function (user) {                                                // 72
    if (!user) {                                                                  // 73
        user = getUser();                                                         // 74
    }                                                                             // 75
    var accountId = getAccountId(user);                                           // 76
    var config = getConfig();                                                     // 77
    var response = httpRequest(config, "status", accountId);                      // 78
    // The latch information is incorrect! Auto unpair                            // 79
    if (_.isObject(response) && isErrorNotPaired(response)) {                     // 80
        Meteor.users.update(                                                      // 81
            {_id: user._id},                                                      // 82
            {$unset:{                                                             // 83
                'services.latch':1,                                               // 84
                'latch':1                                                         // 85
            }}                                                                    // 86
        );                                                                        // 87
        throw new Meteor.Error(500, 'User not paired.');                          // 88
    }                                                                             // 89
    return response.data.operations[config.appId].status === 'off';               // 90
};                                                                                // 91
                                                                                  // 92
Latch.pair = function (token) {                                                   // 93
    check(token, String);                                                         // 94
    var user = getUser();                                                         // 95
    var config = getConfig();                                                     // 96
                                                                                  // 97
    var response = "";                                                            // 98
    try {                                                                         // 99
        response = httpRequest(config, "pair", token);                            // 100
        var accountId = response.data.accountId;                                  // 101
        Meteor.users.update(                                                      // 102
            {_id: user._id},                                                      // 103
            {$set:{                                                               // 104
                'services.latch.accountId':accountId,                             // 105
                'latch':true                                                      // 106
            }}                                                                    // 107
        );                                                                        // 108
        return true;                                                              // 109
    } catch(e) {                                                                  // 110
        console.log(e, response);                                                 // 111
        throw new Meteor.Error(500, 'Error pairing');                             // 112
    }                                                                             // 113
};                                                                                // 114
                                                                                  // 115
Latch.unpair = function () {                                                      // 116
    var user = getUser();                                                         // 117
    var accountId = getAccountId(user);                                           // 118
    var config = getConfig();                                                     // 119
                                                                                  // 120
    var response = "";                                                            // 121
    try {                                                                         // 122
        response = httpRequest(config, "unpair", accountId);                      // 123
        if (_.isObject(response)                                                  // 124
                && (_.isEmpty(response) || isErrorNotPaired(response))) {         // 125
            Meteor.users.update(                                                  // 126
                {_id: Meteor.userId()},                                           // 127
                {$unset:{                                                         // 128
                    'services.latch':1,                                           // 129
                    'latch':1                                                     // 130
                }}                                                                // 131
            );                                                                    // 132
        }                                                                         // 133
        return true;                                                              // 134
    } catch(e) {                                                                  // 135
        console.log(e, response);                                                 // 136
        throw new Meteor.Error(500, 'Error unpairing');                           // 137
    }                                                                             // 138
};                                                                                // 139
                                                                                  // 140
Meteor.methods({                                                                  // 141
    _latchIsLocked: function() {                                                  // 142
        var user = getUser();                                                     // 143
        try {                                                                     // 144
            return Latch.isLocked(user);                                          // 145
        } catch (e) {                                                             // 146
            return true;                                                          // 147
        }                                                                         // 148
    },                                                                            // 149
    _latchPair: Latch.pair,                                                       // 150
    _latchUnpair: Latch.unpair                                                    // 151
});                                                                               // 152
                                                                                  // 153
Accounts.validateLoginAttempt(function (attempt) {                                // 154
    if (!attempt.user || !attempt.user.latch) {                                   // 155
        return true;                                                              // 156
    }                                                                             // 157
    var locked = Latch.isLocked(attempt.user);                                    // 158
    if (locked) {                                                                 // 159
        throw new Meteor.Error(403, 'Account is locked');                         // 160
    }                                                                             // 161
    return true;                                                                  // 162
});                                                                               // 163
// Accounts.addAutopublishFields({forLoggedInUser:['latch']});                    // 164
Meteor.publish("_latchField", function () {                                       // 165
  if (this.userId) {                                                              // 166
    return Meteor.users.find({_id: this.userId}, {fields: {'latch': 1}});         // 167
  } else {                                                                        // 168
    this.ready();                                                                 // 169
  }                                                                               // 170
});                                                                               // 171
                                                                                  // 172
////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['gimco:latch'] = {
  Latch: Latch
};

})();

//# sourceMappingURL=gimco_latch.js.map
